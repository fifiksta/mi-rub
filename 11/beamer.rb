class Slide

	def to_beamer
		slide  = '\begin{frame}'+("{#{@title}}"+"\n" unless @title.nil?)

		slide += '\begin{itemize}'+"\n" unless @items.empty?
		@items.each do |item|
			slide += "\\item #{item}"+"\n"
		end
		slide +=  '\end{itemize}'+"\n" unless @items.empty?

		slide += '\end{frame}'+"\n"
		slide
	end

end


class Presentation


	def to_beamer
		slide  = '\documentclass[12pt]{beamer}'+"\n"
		slide += '\setbeamertemplate{navigation symbols}{}'+"\n"
		slide += '\usetheme{Warsaw}'+"\n"
		slide += "\\title{#{@title}}"+"\n" unless @title.nil?
		slide += "\\author{#{@author}}"+"\n" unless @author.nil?
	
		#\date{\today}
		slide += '\begin{document}'+"\n"
		#slide += '\begin{frame}'+"\n"+' \titlepage'+"\n"+'\end{frame}'+"\n"
		slide += '\frame{\titlepage}'+"\n"

		@slides.each do |s|
			slide += s.to_beamer
		end
		slide += '\end{document}'
		slide
	end


end