#!/bin/ruby

module PresetationStruct

	def var(var)
		define_method(var) do |value|
			instance_variable_set("@#{var}", value)
		end	
	end
end


class Presentation

	extend PresetationStruct

	var :title
	var :author

	def initialize
		@slides = []
	end
	
	def self.fromFile(file)
		code = File.read(file)
		p = Presentation.new
		p.instance_eval(code)
		p
	end

	def slide(&block)
		@slides.push Slide.new(&block)
	end

end


class Slide

	extend PresetationStruct

	var :title

	def initialize(&block)
		@items = []
		instance_eval(&block)
	end

	def item(item)
		@items.push(item)
	end

end


presentation = Presentation.fromFile "./presentation.rb"
require "./beamer.rb"
puts presentation.to_beamer