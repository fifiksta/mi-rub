#!/usr/bin/env ruby

# Roman
class Roman
  MAX_ROMAN = 4999
  FACTORS = [['m', 1000], ['cm', 900], ['d', 500], ['cd', 400],
             ['c', 100], ['xc', 90], ['l', 50], ['xl', 40],
             ['x', 10], ['ix', 9], ['v', 5], ['iv', 4],
             ['i', 1]]

  include Comparable

  def initialize(value)
    if value <= 0 || value > MAX_ROMAN
      fail 'Roman values must be > 0 and <= #{MAX_ROMAN}'
    end
    @value = value
    @roman = to_roman(@value)
  end

  def ==(other)
    other.to_f == to_f
  end

  def +(other)
    Roman.new(@value + other.to_i)
  end

  def *(other)
    Roman.new(@value * other.to_i)
  end

  def -(other)
    Roman.new(@value - other.to_i)
  end

  def /(other)
    Roman.new(@value / other.to_i)
  end

  def <=>(other)
    to_i <=> other.to_i
  end

  def coerce(other)
    [Roman.new(other.to_i), self]
  end

  def eql?(other)
    other == self and other.kind_of? Roman
    other == self && (other.kind_of? Roman)
  end

  def to_s
    @roman
  end

  def to_i
    @value
  end

  def to_f
    @value.to_f
  end

  private

  def to_roman(number)
    return '' if number < 1
    FACTORS.each do |factor|
      if number >= factor.last
        return factor.first + to_roman(number - factor.last)
      end
    end
  end

end
