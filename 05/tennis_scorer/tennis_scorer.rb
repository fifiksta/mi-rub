# -*- encoding : utf-8 -*-
# Tennis Scorer
class TennisScorer

  SCORES = %w(0 15 30 40 A)

  def initialize
    @server = 0
    @receiver = 0
  end

  def score
    s, r = @server, @receiver
    if s >= 4 && r >= 4
      if s == r
        s, r = 3, 3
      else
        s, r = s > r ? [4, 3] : [3, 4]
      end
    end
    "#{SCORES[s]}-#{SCORES[r]}"
  end

  def server_wins
    @server += 1
    test_score
  end

  def receiver_wins
    @receiver += 1
    test_score
  end

  private

  def test_score
    if (@server >= 4 || @receiver >= 4) && (@server - @receiver).abs >= 2
      @server, @receiver = 0, 0
    end
  end

end
