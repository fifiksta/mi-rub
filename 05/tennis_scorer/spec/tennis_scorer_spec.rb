# -*- encoding : utf-8 -*-
require_relative 'spec_helper'
require_relative '../tennis_scorer'

describe TennisScorer do

  subject(:scorer) { TennisScorer.new }

  describe '#score'

    context 'at the beginning' do
      its(:score) { should == '0-0' }
    end

    context 'server wins a point' do
      before do
        subject.server_wins
      end
      its(:score) { should == '15-0' }
    end

    context 'receiver wins a point' do
      before do
        subject.receiver_wins
      end
      its(:score) { should == '0-15' }
    end

    context 'both win a point' do
      before do
        subject.receiver_wins
        subject.server_wins
      end
      its(:score) { should == '15-15' }
    end

    context 'server gets advantage' do
      before do
        3.times {
          subject.receiver_wins
          subject.server_wins
        }
        subject.server_wins
      end
      its(:score) { should == 'A-40' }
    end

    context 'receiver gets advantage' do
      before do
        3.times {
          subject.receiver_wins
          subject.server_wins
        }
        subject.receiver_wins
      end
      its(:score) { should == '40-A' }
    end

    context 'server gets and looses advantage' do
      before do
        4.times {
          subject.receiver_wins
          subject.server_wins
        }
      end
      its(:score) { should == '40-40' }
    end

    context 'server wins game, game resets' do
      before do
        4.times {
          subject.receiver_wins
        }
      end
      its(:score) { should == '0-0' }
    end

end
