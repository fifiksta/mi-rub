require 'minitest/unit'
require 'minitest/autorun'
require_relative '../lib/roman'

class TestRoman < MiniTest::Unit::TestCase
  def test_simple
  	File.open('test/list.txt','r').readlines.each_with_index {|roman,number|
  		assert_equal(roman.strip.downcase, Roman.new(number+1).to_s)
  	}
  end

end
