class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :firstname
      t.string :surname
      t.string :title_before
      t.string :title_after

      t.timestamps
    end
  end
end
