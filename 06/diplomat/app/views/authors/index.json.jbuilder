json.array!(@authors) do |author|
  json.extract! author, :firstname, :surname, :title_before, :title_after
  json.url author_url(author, format: :json)
end
