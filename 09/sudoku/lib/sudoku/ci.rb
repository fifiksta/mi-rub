require 'thor'
require 'sudoku/sudoku'

	class CI < Thor

		desc "solve FILE", "solves provided sudoku"
		option :in
		option :file
		long_desc "This is awesome sudoku solver"

		def solve
			inputs = []
			inputs << options[:in] if options[:in]
			if options[:file]
				File.open(options[:file]).read .split("\n") .each do |line|
					inputs << line
				end
			end

			inputs.each do |input|
				solve_sudoku input
			end


			STDIN.read.split("\n").each do |input|
				solve_sudoku input
			end

		end

		private
		def solve_sudoku(input)
				s = Sudoku.new input
				s.solve
				puts "Solution:"
				puts s.to_s
		end

	end
