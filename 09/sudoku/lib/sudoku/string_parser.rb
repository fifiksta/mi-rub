#!/bin/ruby
require 'sudoku/grid.rb'
# Parse string for 9x9 sudoku game
#
module Sudoku
  class StringParser
    # Static methods will follow
    class << self

      # Return true if passed object
      # is supported by this loader
      def supports?(arg)
        arg.is_a? String and arg.length == 81 and !!(arg =~ /[0-9.]+/)
      end

      # Return Grid object when finished
      # parsing
      def load(arg)
        dimension = Math.sqrt(arg.length).floor
        Grid.new(dimension, arg.split(''))
      end

    end
  end
end