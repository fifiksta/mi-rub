#!/usr/bin/env ruby

# Count number of words in a given files
# (regardless font case)
def word_count(file)
	File.read(file).downcase.split(" ").inject (Hash.new(0)) {|h,word| h[word.gsub(/[^a-z]/, "").to_sym]+=1; h}
end