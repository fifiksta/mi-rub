#!/usr/bin/env ruby

# Find duplication of words in a given
# file and return a Hash indexed by the number
# of lines and containing an Array of 
# words that were repeated
def same_word(file)
	File.read(file)
		.downcase
		.gsub(/[^a-z \n]/, "")
		.split("\n")
		.each_with_index.flat_map { |line,idx| line.split(" ").map {|word| {line: idx+1, word: word} } }
		.inject({hash: Hash.new {|hash,key|hash[key]=[]}, word:""}) { |memo, nxt|
			memo[:hash][nxt[:line]] << nxt[:word] if nxt[:word] == memo[:word]; 
			{hash:memo[:hash], word:nxt[:word]}
		}[:hash]
end