#!/usr/bin/env ruby

# looks for vowels
class VowelFinder < Array
  def initialize(str)
    super(str.downcase.gsub(/[^aeiouy]/, '').split(''))
  end
end

# Summable module
module Summable
  def sum
    reduce :+
  end
end

# Adds Summable to Array
class Array
  include Summable
end
