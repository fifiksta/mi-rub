

def download(url, dest)
  File.open(dest, 'wb') do |fo|
    fo.write open(url).read
  end
  puts "downloaded #{url}"
end

def mkdir(dir)
  Dir.mkdir(dir) unless File.exists?(dir)
end

def extract(target, tmp)
  puts "target: #{target}"
  temp_dir = File.expand_path File.join(File.dirname(__FILE__), '../../tmp')
  puts "downloading to: #{temp_dir}"
  if File.exists?(target)
    Zip::File.open(target) do |zf|
      zf.each do |e|
        zf.extract(e.name, File.join(temp_dir.to_s, e.name))
      end
    end
  else
    exit("file #{target} not found")
  end
end

def invoke(task, *args)
  Rake::Task[task].invoke(*args)
end

def read_content(path, file)
  data = "#{path}/#{file}"
  IO.read(data).force_encoding("ISO-8859-1").encode("utf-8", replace: nil)
end

def import_data(data, model, mapping, separator)
  data.split("\n").each do |line|
    row = line.split(separator)
    t = model.new
    mapping.each_with_index do |key, idx|
      t.send("#{key}=", row[idx])
    end
    t.save!
  end
  import_msg(model)
end

def import_msg(model)
  puts "imported #{model.count} #{model.to_s.downcase.pluralize}"
end

namespace :data do
  desc 'downloads basic 100k dabaset'
  task :fetch do
    dir, dataset = 'tmp', 'ml-100k'
    Dir.mkdir(dir) unless File.exists?(dir)
    target = 'tmp/data.zip'
    download "http://files.grouplens.org/datasets/movielens/#{dataset}.zip", target unless File.exists?(target)
    extract(target, dir) unless Dir.exists?("#{dir}/#{dataset}")
    invoke('data:import')
  end

  desc 'import data from tmp dir'
  task :import do
    dir, dataset = 'tmp', 'ml-100k'
    invoke('db:connect')
    path = "#{dir}/#{dataset}"
    ['genre', 'movie', 'occupation', 'user', 'rating'].each do |table|
      invoke("data:#{table.pluralize}", path) unless table.capitalize.constantize.count > 0
    end
  end

  task :genres, :path do |t, args|
    content = read_content(args[:path], 'u.genre')
    content.split("\n").each do |line|
      data = line.split '|'
      genre = Genre.new
      genre.id = data.last.to_i
      genre.name = data.first.downcase
      genre.save
    end
    
  end

  task :movies, :path  do |t, args|
    content = read_content(args[:path], 'u.item')
    content.split("\n").each do |line|
      data = line.split '|'
      movie = Movie.new
      movie.id = data.shift.to_i
      movie.title = data.shift
      data.shift # rls date
      data.shift # video rls
      data.shift # imdb
      data.each_with_index do |el, id|
        movie.genre << Genre.find(id) if el == "1"
      end
      movie.save
    end
    
  end

  task :occupations, :path  do |t, args|
    content = read_content(args[:path], 'u.occupation')
    content.split("\n").each_with_index do |line, id|
      o = Occupation.new
      o.id = id + 1
      o.title = line
      o.save
    end
  end

  task :users, :path  do |t, args|
    content = read_content(args[:path], 'u.user')
    content.split("\n").each_with_index do |line, id|
      data = line.split '|'
      user = User.new
      user.id = data.shift.to_i
      user.age = data.shift.to_i
      user.gender = data.shift
      user.occupation = Occupation.find_by_title(data.shift)
      user.zip = data.shift
      user.save
    end
  end

  task :ratings, :path  do |t, args|
    content = read_content(args[:path], 'u1.base')
    content.split("\n").each do |line|
      data = line.split "\t"
      r = Rating.new
      r.user_id = data.shift.to_i
      r.movie_id = data.shift.to_i
      r.rating = data.shift.to_i
      r.timestamp = DateTime.strptime(data.shift,"%s")
      r.save
    end
  end
end
