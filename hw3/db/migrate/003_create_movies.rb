class CreateMovies < ActiveRecord::Migration

  def up
    create_table :movies do |t|
      t.string :title
    end

    create_table :genres_movies do |t|
      t.belongs_to :genre
      t.belongs_to :movie
    end
    
  end

  def down
    drop_table :movies
  end

end