class CreateOccupations < ActiveRecord::Migration

  def up
    create_table :occupations do |t|
      t.string :title
    end

    change_table :users do |t|
      t.belongs_to :occupation
    end
  end

  def down
    drop_table :occupations
  end

end