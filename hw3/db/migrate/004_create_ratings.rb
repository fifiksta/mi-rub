class CreateRatings < ActiveRecord::Migration

  def up
    create_table :ratings do |t|
      t.integer :rating
      t.belongs_to :user
      t.belongs_to :movie
      t.timestamp :timestamp
    end  
  end

  def down
    drop_table :ratings
  end

end