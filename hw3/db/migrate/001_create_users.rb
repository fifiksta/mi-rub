class CreateUsers < ActiveRecord::Migration

  def up
    create_table :users do |t|
      t.string :gender
      t.integer :age
      t.string :zip
    end
  end

  def down
    drop_table :users
  end

end