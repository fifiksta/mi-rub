class Genre < ActiveRecord::Base

  validates :name, presence: true
  has_and_belongs_to_many :movie

  def is_genre?(genre)
    @name == genre
  end

end