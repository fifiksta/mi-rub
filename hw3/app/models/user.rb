class User < ActiveRecord::Base

  validates :age, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :gender, inclusion: ['F', 'M']
  belongs_to :occupation


  def zip #damn.. should be string
    super.to_s
  end

end