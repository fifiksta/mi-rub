class Occupation < ActiveRecord::Base

  validates :title, presence: true
  has_many :user

end