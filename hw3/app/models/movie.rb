class Movie < ActiveRecord::Base

  validates :title, presence: true
  has_and_belongs_to_many :genre
  has_many :rating

  def is_genre?(gen)
    genre.any? { |g| gen == g.name}
  end

end