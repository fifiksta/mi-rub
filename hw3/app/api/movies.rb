require 'yajl/json_gem'
require 'grape'

#ActiveRecord::Base.logger = Logger.new(STDOUT)

module Movies

  class MoviesAPI < Grape::API

    version 'v1', using: :path

    resource 'movies' do
      

      params do
        optional :limit, type: Integer, default: 20
        optional :offset, type: Integer, default: 0
      end

      get do
        Movie.limit(params[:limit])
             .offset(params[:offset])
      end

      get "top" do
        
        Movie.limit(params[:limit])
             .offset(params[:offset])
             .joins(:rating)
             .select("movies.*, avg(ratings.rating) as score")
             .group("movies.id")
             .order("score desc")
      end

      
      namespace :show do

        params do
          requires :movie_id, type: Integer, desc: "A movie ID."
        end

        get ":movie_id" do
          Movie.find(params[:movie_id])
        end

      end

      namespace :genre do

        params do
          requires :genre_name, type: String
        end

        get ":genre_name" do
          genre = Genre.find_by_name(params[:genre_name])
          unless genre.nil?
            genre.movie
          else
            []
          end
        end

      end

      namespace :search do
        
        params do
          requires :title, type: String
        end

        get ":title" do
          Movie.where("title ilike ?", "%#{params[:title]}%")
        end
      end

      namespace :rating do
        
        params do
          requires :movie_id, type: Integer
        end

        get ":movie_id" do
          Movie.joins(:rating)
              .select("movies.id as movie_id, movies.title as title,  avg(ratings.rating) as rating_avg")
              .where("movies.id" => params[:movie_id])
              .group("movies.id")             
        end
      end

    end
  end
end