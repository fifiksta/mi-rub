require 'yajl/json_gem'
require 'grape'

module Movies
  class OccupationAPI < Grape::API

    version 'v1', using: :path

    resource 'occupations' do

      params do
        optional :limit, type: Integer, default: 20
        optional :offset, type: Integer, default: 0
      end

      get do
        Occupation.limit(params[:limit])
                .offset(params[:offset])
                .order(:id)
      end

      params do
        requires :id, type: Integer
      end

      get ":id" do
        Occupation.find(params[:id])
      end

      get ":id/users" do
        Occupation.find(params[:id]).user
      end

    end

  end
end