require 'yajl/json_gem'
require 'grape'

module Movies
  class UsersAPI < Grape::API

    version 'v1', using: :path

    resource 'users' do

      params do
        optional :limit, type: Integer, default: 20
        optional :offset, type: Integer, default: 0
      end

      get do
        User.limit(params[:limit])
             .offset(params[:offset])
      end

      params do
        requires :id, type: Integer
      end

      get ":id" do
        User.find(params[:id])
      end

    end

  end
end