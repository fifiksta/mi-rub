#!/bin/ruby
require_relative 'grid.rb'
require_relative 'string_parser.rb'

# Basic sudoku solver
class Sudoku

  PARSERS = [StringParser]

  def initialize(game)
    @grid = load(game)
  end

  # Return true when there is no missing number
  def solved?
    @grid.missing == 0
  end

  # Solves sudoku and returns 2D Grid
  def solve
    fail 'invalid game given' unless @grid.valid?
    solver
  end

  def solver(grid = @grid, index = 0)
    grid.eliminate if index == 0
    @grid = grid if index == 81 && grid.valid?
    if index < 81
      cell = grid[index / 9, index % 9]
      if cell.filled?
        solver(grid, index + 1)
      else
        fork(grid, cell, index)
      end
    end
  end

  def fork(grid, cell, index)
    cell.values.each do |value|
      g = grid.clone
      g.set_value_r(cell.row, cell.col, value)
      solver(g, index + 1)
      return if solved?
    end
  end

  def solution
    @grid.solution
  end

  def to_s
    @grid.to_s
  end

  protected

  def load(game)
    PARSERS.each do |p|
      return p.load(game) if p.supports?(game)
    end
    fail "input '#{game.to_s}' is not supported yet"
  end

end
