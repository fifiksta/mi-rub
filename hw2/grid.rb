#!/usr/bin/env ruby

# Methods for Grid values elimination
module GridEliminator

  def eliminate
    each do |cell|
      set_value_r(cell.row, cell.col, cell.value) if cell.filled?
    end
  end

  def set_value_r(row, col, value)
    self[row, col] = value
    self[row, col].values = [value]
    ex = proc { |cell| cell.exclude(value) }
    row_elems(row, &ex)
    col_elems(col, &ex)
    block_elems(col, row, &ex)
    set_single_values(row, col, value)
  end

  def set_single_values(row, col, value)
    try_set = proc do |cell|
      if !cell.filled? && cell.has_single_value?
        set_value_r(cell.row, cell.col, cell.values.first)
      end
    end

    row_elems(row, &try_set)
    col_elems(col, &try_set)
    block_elems(col, row, &try_set)
  end

  def clone
    g = Grid.new(dimension)
    g.data.each do |cell|
      ocell = self[cell.row, cell.col]
      cell.value = ocell.value
      cell.values = Array.new(ocell.values)
    end
    g
  end

end

# Contains sudoku game board
class Grid

  attr_reader :dimension
  alias_method :rows, :dimension
  alias_method :cols, :dimension

  include GridEliminator

  # Created Sudoku game grid of given dimension
  def initialize(dim, data = Array.new(dim * dim, 0))
    @data = data.each_with_index.map do |value, i|
      Cell.new(value == '.' ? 0 : value.to_i, self, i / dim, i % dim)
    end
    @dimension = dim
  end

  # Return string with game board in a console friendly format
  def to_s(width = 3)
    res = spliters = (0...(cols / 3)).map { '-' * 9 }.join('+') + "\n"
    each.with_index do |cell, position|
      row, col = position / cols, position % cols
      res += " #{cell.to_s} "
      res += "\n" if col == cols - 1
      res += spliters if col == cols - 1 && row % 3 == 2
      res += '|' if col % 3 == 2 && col != cols - 1
    end
    res
  end

  # First element in the sudoku grid
  define_method(:first) { @data.first }

  # Last element in the sudoku grid
  define_method(:last) { @data.last }

  # Return value at given position
  define_method(:value) { |row, col| self[row, col] }

  # Marks number +z+ which shouldn't be at position [x, y]
  def exclude(row, col, value)
    self[row, col].exclude value
  end

  # True when there is already a number
  define_method(:filled?) { |row, col| self[row, col].filled }

  # True when no game was loaded
  define_method(:empty?) { filled == 81 }

  # Yields elements in given row
  def row_elems(row)
    return to_enum(__method__, row) unless block_given?
    (0...cols).each { |col| yield(self[row, col]) }
  end

  # Yields elements in given column
  def col_elems(col)
    return to_enum(__method__, col) unless block_given?
    (0...rows).each { |row| yield(self[row, col]) }
  end

  # Yields elements from block which is
  # containing element at given position
  def block_elems(x, y)
    return to_enum(__method__, x, y) unless block_given?
    x, y = [x, y].map { |e| e / 3 * 3 }
    (y...(y + 3)).each do |row|
      (x...(x + 3)).each { |col| yield(self[row, col]) }
    end
  end

  # With one argument return row, with 2, element
  # at given position
  def [](*args)
    if args.length == 2
      y, x = args
      @data[y * cols + x]
    else
      Row.new(@data.slice(args[0] * cols, cols))
    end
  end

  # With one argument sets row, with 2 element
  def []=(*args)
    if args.length == 3
      y, x, val = args
      @data[y * cols + x].value = val
    else
      row, values = args
      (0..cols).each do |col|
        @data[row * cols + col].value = values[col]
      end
    end
  end

  # Return number of missing numbers in grid
  def missing
    @dimension * @dimension - filled
  end

  # Number of filled cells
  def filled
    @data.reduce(0) { |a, e| e.filled? ? a + 1 : a }
  end

  # Iterates over all elements, left to right, top to bottom
  def each(&block)
    @data.each(&block)
  end

  # Return true if no filled number break sudoku rules
  def valid?
    row, col = -1, -4
    !(0...9).all? do
      row, col = row + 1, (col + 4) % 9
      return valid_cell?(row, col)
    end
  end

  def valid_cell?(row, col)
    test_set(row_elems(row)) &&
    test_set(col_elems(col)) &&
    test_set(block_elems(col, row))
  end

  # Serialize grid values to a one line string
  def solution
    @data.map { |x| x.to_i }.join ''
  end

  protected

  attr_reader :data

  private

  def test_set(x)
    t = []
    x.each do |cell|
      next unless cell.filled?
      return false if t.include? cell.to_i
      t.push cell.to_i
    end
    true
  end

end

# Cell
class Cell

  attr_accessor :value, :row, :col, :values

  def initialize(value, grid, row, col)
    @value, @row, @col = value, row, col
    @grid = grid
    if @value == 0
      @values = (1..9).to_a
    else
      @values = [@value]
    end
  end

  def exclude(value)
    @values.delete value
  end

  def has_value?
    !@values.empty?
  end

  def has_single_value?
    @values.length == 1
  end

  def excluded?(value)
    @values.any? { |val| val == value }
  end

  def filled?
    @value != 0
  end

  def to_i
    @value
  end

  def to_s
    filled? ? @value.to_s : ' '
  end

  def repr
    "(#{row}, #{col}) #{filled?} #{value} [#{@values.join(',')}]"
  end

  def ==(other)
    if other.is_a? Fixnum
      @value == other
    elsif other.is_a? Cell
      value == other.value
    else
      false
    end
  end

  def hashcode
    hash
  end

end

# Row
class Row < Array

  def []=(i, value)
    self[i].value = value
  end

end
