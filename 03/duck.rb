#!/usr/bin/env ruby

# @TODO: define class Duck with constructor
# that takes as parameters `name` and `age` of
# the duck

class Duck

  attr_reader :name, :flying, :swimming, :num_quacks
  alias_method :flying?, :flying
  alias_method :swimming?, :swimming

  @@num_quacks = 0

  def initialize(name, age)
    @name, @age = name, age
    @flying = false
    @swimming = false
    @num_quacks = 0
  end

  def quack
    @@num_quacks += 2
    @num_quacks  += 2
    'quack quack'
  end

  def self.can_fly
    true
  end

  def self.can_swim
    true
  end

  def flying=(flying)
    @flying   = flying
    @swimming = false
  end

  def fly!
    @flying = true
  end

  def swimming=(swimming)
    @swimming = swimming
    @flying = false
  end

  def compare(duck)
    age <=> duck.age
  end

  def self.num_quacks
    @@num_quacks
  end

  protected

  attr_reader :age

end
