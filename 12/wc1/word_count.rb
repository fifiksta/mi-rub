require 'thread'
require 'fiber'

def word_count(file)
  queue = Queue.new
  counts = Hash.new(0)

  mutex = Mutex.new
  threads = []
  producer_count = 1

  10.times do
    threads << Thread.new do
      loop do
        break if queue.empty? && producer_count == 0
        counts[queue.pop] += 1 unless queue.empty?
      end
    end
  end

    # TODO write counter
  File.foreach(file) do |line|
    producer_count += 1
    threads << Thread.new do
        line.scan(/\w+/) do |word|
          queue << word.downcase
      end
      producer_count -= 1
    end
  end


  fibers_p = []
  fibers_c = []
  fiber_i = 0
  fibers_c << Fiber.new do
        counts[queue.pop] += 1 unless queue.empty?
        fibers_p.sample.transfer
    end
  end

  File.foreach(file) do |line|
    fibers_p << Fiber.new do
        line.scan(/\w+/) do |word|
          queue << word.downcase
          fibers_c.sample.transfer
      end
      fibers_p.delete_if { |f| f == self }
      fibers_p.sample.transfer
    end
  end

  fiber_p.sample.resume


  producer_count -= 1

  threads.each(&:join)

  counts.keys.sort.each do |k|
    print "#{k}:#{counts[k]} "
  end
  counts
end