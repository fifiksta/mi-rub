#!/usr/bin/env ruby

# Immitates some behaviour of the Array
# clas
class MyArray

  @car = nil
  @cdr = nil

  def initialize(ary = [])
    if ary.length > 0
      @car = ary.first
      @cdr=MyArray.new(ary.drop(1))
    end
  end

  def car
    @car
  end

  def car=(val)
    @car = val
  end

  def cdr
    @cdr = MyArray.new if @cdr == nil
    @cdr
  end

  def cdr=(ary)
    ary = MyArray.new(ary) if ary.is_a? Array
    @cdr = ary
  end

  def nil?
    @car == nil
  end

  def clear
    @car = nil
    @cdr = nil
  end

  def to_s
    "("+car+" . "+@cdr+")"
  end

  def [](i)
    return @car if i == 0
    return @cdr[i-1]
  end

  def size
    nil? ? 0 : @cdr.size + 1
  end
  
  def pop
    if cdr.nil?
      value = @car
      clear
      return value
    end
    return cdr.pop()
  end

  def push(val)
    if cdr.nil?
      cdr=val
    else
      push(cdr)
    end
  end

  def to_a()
    ary = self
    array = []
    while not ary.nil?
      array.push(ary.car)
      ary=ary.cdr
    end
    array
  end

  def reduce(val = nil)
    arr = self
    while not arr.nil?
      val = yield(val, arr.car)
      arr = arr.cdr
    end
    return val
  end

  def map()
    return to_enum(__method__) unless block_given?
    ary = self
    nary = MyArray.new
    cary = nary
    while not ary.nil?
      cary.car = yield ary.car
      cary=cary.cdr
      ary=ary.cdr
    end
    nary.to_a
  end

  def each
    return to_enum(__method__) unless block_given?
    ary = self
    while not ary.nil?
      yield ary.car
      ary = ary.cdr
    end
  end

  def reverse(my_array = false)
    reversed = reduce(MyArray.new) do |prev,nxt|
      ary = MyArray.new
      ary.car = nxt
      ary.cdr = prev
      next ary
    end
    return (my_array ? reversed : reversed.to_a)
  end

  def reverse!
    reversed = reverse(true)
    @car = reversed.car
    @cdr = reversed.cdr
    reversed
  end

  def reverse_each
    return to_enum(__method__) unless block_given?
    reverse(true).each { |x|
      yield x
    }
  end

  def select
    selection = MyArray.new
    sel = selection
    ary = self
    while not ary.nil?
      if yield(ary.car)
        sel.car = ary.car
        sel = sel.cdr
      end
      ary=ary.cdr
    end
    selection.to_a
  end

  def include? (elem)
    if elem == car
      true
    elsif nil?
      false
    else
      cdr.include? elem
    end
  end

  def max
    reduce(0) do |prv,nxt|
      pval = prv.is_a?(String) ? prv.length : prv
      nval = nxt.is_a?(String) ? nxt.length : nxt
      next (pval > nval) ? prv : nxt
    end
  end

  alias_method :collect, :map

end
