class OpenStruct

	def initialize
		@data = Hash.new
	end

	def method_missing(method_name, *args)
		if method_name[-1] == '='
			@data[method_name[0...-1]] = args.first
		else
			@data[method_name.to_s]
		end
	end

end