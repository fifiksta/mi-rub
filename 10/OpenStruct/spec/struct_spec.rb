require_relative '../struct.rb'


describe OpenStruct do

	it "should work" do
		s = OpenStruct.new
		s.test = 1
		expect(s.test).to eql 1
	end

end