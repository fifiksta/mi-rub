#include <ruby.h>
#include <math.h>


static double euclidean(VALUE self, VALUE other_array) {
  double value = 0.0;
  unsigned int index;
  unsigned long vector_length = (RARRAY_LEN(self) - 1);

  if (RARRAY_LEN(self) != RARRAY_LEN(other_array)){
    rb_raise(rb_eArgError, "vector sizes doesn't match");
  }


  for(index = 0; index <= vector_length; index++) {
    double x, y;

    x = NUM2DBL(RARRAY_PTR(self)[index]);
    y = NUM2DBL(RARRAY_PTR(other_array)[index]);

    value += pow(x - y, 2);
  }
  return sqrt(value);
}

/*
module MlDistance
  def euclidean(other)
    sum = 0.0
    self.each_index do |i|
      sum += (self[i] - other[i])**2
    end
    Math.sqrt(sum)
  end
end
*/
static VALUE rb_euclidean(VALUE self, VALUE other_array) {
  return rb_float_new(euclidean(self, other_array));
}


static double dot_product(VALUE self, VALUE other_array){
  double value = 0.0;
  unsigned int index;
  unsigned long vector_length = (RARRAY_LEN(self) - 1);

  if (RARRAY_LEN(self) != RARRAY_LEN(other_array)){
    rb_raise(rb_eArgError, "vector sizes doesn't match");
  }

  for(index = 0; index <= vector_length; index++) {
    value += NUM2DBL(RARRAY_PTR(self)[index]) * NUM2DBL(RARRAY_PTR(other_array)[index]);
  }

  return value;
}
/*
module MlDistance
  def dot_product(ary)
    zip(ary).map { |a,b| a*b } .reduce(:+)
  end
end
*/
static VALUE rb_dot_product(VALUE self, VALUE other_array) {
  return rb_float_new(dot_product(self, other_array));
}

/*
module MlDistance
  def cosine(ary)
    dot_product(ary) / (self.abs * ary.abs)
  end
end
*/
static VALUE rb_cosine(VALUE self, VALUE other_array){
  return rb_float_new(dot_product(self, other_array) / (sqrt(dot_product(self,self)) * sqrt(dot_product(other_array,other_array))));
}

void Init_ml_distance() {
  VALUE ml_distance = rb_define_module("MlDistance");
  rb_define_method(ml_distance, "euclidean", rb_euclidean, 1);
  rb_define_method(ml_distance, "dot_product", rb_dot_product, 1);
  rb_define_method(ml_distance, "cosine", rb_cosine, 1);
}