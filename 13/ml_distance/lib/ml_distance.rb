require 'ml_distance/version'
require 'ml_distance/ml_distance'
require 'ml_distance/core_ext/array'

module MlDistance
  
=begin  
  def dot_product(ary)
    zip(ary).map { |a,b| a*b } .reduce(:+)
  end

  def abs
    euclidean(Array.new(self.length, 0))
  end


  def cosine(ary)
    dot_product(ary) / (self.abs * ary.abs)
  end
=end
end
